package com.example.physicsclickerapp;

public class get_set_vars {
	
	
	public static  int session_id = 21 ;
	public static  int questionNumber = 7;
	public static String result_message = "";
	
	
	public static void setSessionId(int sId)
	{
		sId = session_id;
	}
	
	
	public static void setQuestionNumber(int qnum)
	{
		qnum = questionNumber;
	}

	
	public static int getSessionId()
	{
		return session_id;
	}
	
	public static int getQuestionNumber()
	{
		return questionNumber;
	}
	
	public static void setResultString(String result)
	{
		result = result_message;
	}
	
	public static String getResultString()
	{
		return result_message;
	}


}
