package com.example.physicsclickerapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.net.Uri;
import android.util.Log;


public class GetResponseFromService
{
	
	protected static String URL = "http://50.116.43.100/physicsapp/api.php";
	protected static String encodedData = "";
	
	public static String convertResponse(InputStream is) {

	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append((line + "\n"));
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
	
	
	public static  HttpResponse SendHttpPost(JSONObject jsonData) 
	{

      
       try
       {
        HttpClient client = new DefaultHttpClient();
        HttpGet httpRequest = new HttpGet();
        encodedData = Uri.encode(jsonData.toString(), "UTF-8");
        httpRequest.setURI(new URI(URL + "/?request=" + encodedData));
        HttpResponse response = client.execute(httpRequest);
        Log.v("Response From Service", response.toString());
        
        return response;
       }
       catch (ClientProtocolException e)
       {
                // TODO Auto-generated catch block
    	   e.printStackTrace();
       } 
       catch (UnsupportedEncodingException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
           
       }
       catch (IOException e)
      {
                // TODO Auto-generated catch block
                e.printStackTrace();
      } catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
       
        
       


       return null;
	}
	
}