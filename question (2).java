package com.example.physicsclickerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class question extends Activity{
	
	String questionNumber;
	int question = 0;
	EditText q;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question);
	}
	
	
	
	public void submitQuestion(View view)
	{
		
		q  = (EditText)findViewById(R.id.question_entry);
		questionNumber = q.getText().toString();
		
		
		//use get set vars to set question_number
    	try{
    		question = Integer.parseInt(questionNumber, 10);
        	Log.d("Question number is ", questionNumber);
    		get_set_vars.questionNumber = question;
        }catch(NumberFormatException nfe){
        	Toast.makeText(this, "Please Enter an Integer Value", Toast.LENGTH_LONG).show();
        	startActivity(new Intent(this,question.class));
        }
    	if(questionNumber.isEmpty()==false)
    	{
		startActivity(new Intent(this,Answer.class));
		finish();
    	}
	
	}
	
	public void exit(View view)
	{
		finish();
		
	}

}

