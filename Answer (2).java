package com.example.physicsclickerapp;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.physicsclickerapp.GetResponseFromService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;


public class Answer extends Activity {
	
	RadioButton a, b, c, d,e;
	String android_id = "";
	int androidId = 0;
	int Session_id = 0;
	
	//Holder Variables
	//int Session_id = 1;
	String msg = "";
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answer);
        
        
        
         a = (RadioButton) findViewById(R.id.radio0);
    	 b = (RadioButton) findViewById(R.id.radio1);
    	 c = (RadioButton) findViewById(R.id.radio2);
    	 d = (RadioButton) findViewById(R.id.radio3);
    	 e = (RadioButton) findViewById(R.id.radio4);


		
        RadioGroup radiogroup = (RadioGroup) findViewById(R.id.radioGroup1);
        radiogroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
        	public void onCheckedChanged(RadioGroup group, int checkedId)
        	{
        		
        		if (a.isChecked())
        		{ DisplayToast("You selected A");}
        		else if (b.isChecked())
        		{DisplayToast("You selected B");}
        		else if (c.isChecked())
        		{DisplayToast("You selected C");}
        		else if (d.isChecked())
        		{DisplayToast("You selected D");}
        		else if (e.isChecked())
        		{DisplayToast("You selected E");}
        		else {
        		DisplayToast("You made no selection in the time limit. Default selection submitted.");}
        	
        	}
        });
        
        
    }
	
	public JSONObject JsonObj(int session_Id,int question_no, String result,  String output) throws JSONException
		{
	        int request = 2;
	        JSONObject j = new JSONObject();
			j.put("requestType", request);
			j.put("sp_Name", "sp_Save_Clicker_Response");
			List <Object> list = new ArrayList <Object> ();
					list.add(session_Id);
					list.add(question_no);
					list.add(result);
					list.add(output);
				JSONArray array = new JSONArray();
					for(int i = 0; i <list.size(); i++)
					{
						array.put(list.get(i));
					}
				
		
					j.put("args", array);
		
			 Log.v("ClickerApp", "Request JSON :" + " " + j);
			
			return j;
		}
	    
    public void DisplayToast(String msg)
    {
    	//post it to a toast message
    	Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
    
  
 // this is the function that gets called when you click the button
    public void send()
    {
    	// make sure the fields are not empty
    	msg = "test";
    	// get the message from the radioButtons
    	if (a.isChecked())
    		{
    			msg = a.getText().toString();
    	 	}
    	else if(b.isChecked())
    		{
    			msg = b.getText().toString(); 
    	    }
    	else if (c.isChecked())
    		{
    	    	msg = c.getText().toString();
    	    }
    	else if (d.isChecked())
    	   	{
    	    	msg = d.getText().toString();
    	    }
    	else if (e.isChecked())
    		{
    			msg = e.getText().toString();
    	    }
    	else
    		{
    			msg = "F";
    		}
    	
    	
    	        if (msg.length()>0)
    	        {
    	        	try 
    	        	{
    	        		JSONObject jSon = JsonObj(get_set_vars.getSessionId(),get_set_vars.getQuestionNumber(),msg,""); //creates JSON object. Parameters are to be extracted form previous activities.
    	        		GetResponseFromService.SendHttpPost(jSon); 
        		  	}
    	        	
    	        	 catch (JSONException e1) {
    	        		// TODO Auto-generated catch block
    	        		e1.printStackTrace();
    	        	} 
    	        	catch (Exception except)
    	        	{
    	        		
    	        	}
   	        }
    	}
  
        
    public void submit(View view)
    {
    	send();
    	DisplayToast("YOUR AMSWER HAS BEEN SUBMITTED!");
    	startActivity(new Intent(this,question.class));
    	//Handle to submit answer
    	finish();
    }
    
    public void exitApp(View view)
	{
		finish();
		
	}
    
    
    
}
