package com.example.physapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {
	
	String sessionkey = "KEY"; 
	int session_ID = 0; 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        setContentView(R.layout.activity_main);        
    }
    
    
    public void enter(View view)
	{
    	EditText sessionIDView = (EditText)findViewById(R.id.session_id);
    	String sid = sessionIDView.getText().toString();
    	
    	
        try{
        	//use get set vars to set sessio_Id
        	session_ID = Integer.parseInt(sid, 10);
        	Log.d("Session Id is ", sid);
        	
            get_set_vars.session_id = session_ID;

        }
        catch(NumberFormatException nfe){
        	Toast.makeText(this, "Please Enter an Integer Value", Toast.LENGTH_LONG).show();
        	Log.d("Session Id is ",sid);
        	//startActivity(new Intent(this,MainActivity.class));
        	
        }
        //Start the questionActivity
        if(sid.isEmpty()==false){
        	Log.d("Session Id is ",sid);
        	startActivity(new Intent(this,question.class));
        	finish();
        }
	}
    
}
